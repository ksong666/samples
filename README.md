# sample

#### 介绍

Ascend sample，请根据自己的需要进入对应文件夹获取应用，或者点击下面的说明链接选择需要的应用。


#### 链接地址


<details open><summary>classification：基于googlenet的分类应用，输入为图片，输出为图片。</summary><blockquote>

- [for_atlas200dk_1.7x.0.0_c++](https://gitee.com/ascend/samples/tree/master/classification/for_atlas200dk_1.7x.0.0_c++)：该分支是运行在200DK上基于1.7x.0.0版本的C++样例。使用opencv对图像进行预处理，对预处理后的图像中的物体进行分类，最后通过opencv进行相应后处理。  
- [for_atlas200dk_1.7x.0.0_python](https://gitee.com/ascend/samples/tree/master/classification/for_atlas200dk_1.7x.0.0_python)：该分支是运行在200DK上基于1.7x.0.0版本的python样例。使用dvpp对图像进行预处理，对预处理后的图像中的物体进行分类，最后通过pillow进行相应后处理。   
- [for_atlas300_1.7x.0.0_c++](https://gitee.com/ascend/samples/tree/master/classification/for_atlas300_1.7x.0.0_c++)：该分支是运行在ai1环境上基于1.7x.0.0版本的C++样例。使用opencv对图像进行预处理，对预处理后的图像中的物体进行分类，最后通过opencv进行相应后处理。 
</blockquote></details>  


<details open><summary>classification_video：基于googlenet的分类应用，输入为视频，输出为视频。</summary><blockquote>

- [for_atlas200dk_1.7x.0.0_c++](https://gitee.com/ascend/samples/tree/master/classification_video/for_atlas200dk_1.7x.0.0_c++)：该分支是运行在200DK上基于1.7x.0.0版本的C++样例。使用opencv对视频帧进行预处理，对预处理后的视频帧中的物体进行分类，最后通过opencv进行相应后处理。   
- [for_atlas300_1.7x.0.0_c++](https://gitee.com/ascend/samples/tree/master/classification_video/for_atlas300_1.7x.0.0_c++)：该分支是运行在ai1环境上基于1.7x.0.0版本的C++样例。使用opencv对视频帧进行预处理，对预处理后的视频帧中的物体进行分类，最后通过opencv进行相应后处理。   
</blockquote></details>


<details open><summary>colorization：基于alexnet的黑白图像上色应用，输入为图片，输出为图片。</summary><blockquote>

- [for_atlas200dk_1.7x.0.0_c++](https://gitee.com/ascend/samples/tree/master/colorization/for_atlas200dk_1.7x.0.0_c++)：该分支是运行在200DK上基于1.7x.0.0版本的C++样例。使用opencv对图像进行预处理，对预处理后的图像进行色彩通道预测，最后通过opencv进行相应后处理。  
- [for_atlas200dk_1.7x.0.0_python](https://gitee.com/ascend/samples/tree/master/colorization/for_atlas200dk_1.7x.0.0_python)：该分支是运行在200DK上基于1.7x.0.0版本的python样例。使用opencv对图像进行预处理，对预处理后的图像进行色彩通道预测，最后通过opencv进行相应后处理。
- [for_atlas300_1.7x.0.0_c++](https://gitee.com/ascend/samples/tree/master/colorization/for_atlas300_1.7x.0.0_c++)：该分支是运行在200DK上基于1.7x.0.0版本的C++样例。使用opencv对图像进行预处理，对预处理后的图像进行色彩通道预测，最后通过opencv进行相应后处理。
  
</blockquote></details>  

   
<details open><summary>colorization_video：基于alexnet的黑白图像上色应用，输入为视频，输出为视频。</summary><blockquote>

- [for_atlas200dk_1.7x.0.0_c++](https://gitee.com/ascend/samples/tree/master/colorization_video/for_atlas200dk_1.7x.0.0_c++)：该分支是运行在200DK上基于1.7x.0.0版本的C++样例。使用opencv对视频帧进行预处理，对预处理后的视频帧进行色彩通道预测，最后通过opencv进行相应后处理。  
- [for_atlas300_1.7x.0.0_c++](https://gitee.com/ascend/samples/tree/master/colorization_video/for_atlas300_1.7x.0.0_c++)：该分支是运行在200DK上基于1.7x.0.0版本的C++样例。使用opencv对视频帧进行预处理，对预处理后的视频帧进行色彩通道预测，最后通过opencv进行相应后处理。
</blockquote></details>


<details open><summary>objectdetection：基于yolov3的目标检测应用，输入为图片，输出为图片。</summary><blockquote>

- [for_atlas200dk_1.7x.0.0_c++](https://gitee.com/ascend/samples/tree/master/objectdetection/for_atlas200dk_1.7x.0.0_c++)：该分支是运行在200DK上基于1.7x.0.0版本的C++样例。使用opencv对图像进行预处理，对预处理后的图像中的物体进行目标检测，最后通过opencv进行相应后处理。  
- [for_atlas200dk_1.7x.0.0_python](https://gitee.com/ascend/samples/tree/master/objectdetection/for_atlas200dk_1.7x.0.0_python)：该分支是运行在200DK上基于1.7x.0.0版本的python样例。使用dvpp对图像进行预处理，对预处理后的图像中的物体进行目标检测，最后通过pillow进行相应后处理。   
- [for_atlas300_1.7x.0.0_c++](https://gitee.com/ascend/samples/tree/master/objectdetection/for_atlas300_1.7x.0.0_c++)：该分支是运行在ai1环境上基于1.7x.0.0版本的C++样例。使用opencv对图像进行预处理，对预处理后的图像中的物体进行目标检测，最后通过opencv进行相应后处理。  
</blockquote></details>


<details open><summary>objectdetection_video：基于yolov3的目标检测应用，输入为视频，输出为视频。</summary><blockquote>

- [for_atlas200dk_1.7x.0.0_c++](https://gitee.com/ascend/samples/tree/master/objectdetection_video/for_atlas200dk_1.7x.0.0_c++)：该分支是运行在200DK上基于1.7x.0.0版本的C++样例。使用opencv对视频帧进行预处理，对预处理后的视频帧中的物体进行目标检测，最后通过opencv进行相应后处理。    
- [for_atlas300_1.7x.0.0_c++](https://gitee.com/ascend/samples/tree/master/objectdetection_video/for_atlas300_1.7x.0.0_c++)：该分支是运行在ai1环境上基于1.7x.0.0版本的C++样例。使用opencv对视频帧进行预处理，对预处理后的视频帧中的物体进行目标检测，最后通过opencv进行相应后处理。  
</blockquote></details>

<details open><summary>facedetection：基于caffe-ssd的人脸检测应用，输入为树莓派摄像头，输出为视频。</summary><blockquote>

- [for_atlas200dk_1.7x.0.0_c++](https://gitee.com/ascend/samples/tree/master/facedetection/for_atlas200dk_1.7x.0.0_c++)：该分支是运行在200DK上基于1.7x.0.0版本的C++样例。使用dvpp对视频帧进行预处理，对预处理后的视频帧进行人脸检测，最后进行相应后处理。  
- [for_atlas200dk_1.7x.0.0_python](https://gitee.com/ascend/samples/tree/master/facedetection/for_atlas200dk_1.7x.0.0_python)：该分支是运行在200DK上基于1.7x.0.0版本的python样例。使用dvpp对视频帧进行预处理，对预处理后的视频帧进行人脸检测，最后进行相应后处理。 
</blockquote></details> 

<details open><summary>mark_detection：基于yolov3的口罩识别应用，输入为图片，输出为图片。</summary><blockquote>

- [for_atlas200dk_1.7x.0.0_python](https://gitee.com/ascend/samples/tree/master/mark_detection/%20for_atlas200dk_1.7x.0.0_python)：该分支是运行在200DK上基于1.7x.0.0版本的python样例。使用opencv和pillow对图像进行预处理，对预处理后的图像进行口罩识别，最后通过opencv和pillow进行相应后处理。 
- [for_atlas200dk_1.3x.0.0_python](https://gitee.com/ascend/samples/tree/master/mark_detection/for_atlas200dk_1.3x.0.0_python)：该分支是运行在200DK上基于1.3x.0.0版本的python样例。使用opencv对图像进行预处理，对预处理后的图像进行口罩识别，最后通过opencv进行相应后处理。 
</blockquote></details>


<details open><summary>mark_detection_video：基于yolov3的口罩识别应用，输入为视频，输出为视频。</summary><blockquote>

- [for_atlas200dk_1.3x.0.0_c++](https://gitee.com/ascend/samples/tree/master/mark_detection_video/for_atlas200dk_1.3x.0.0_c++)：该分支是运行在200DK上基于1.3x.0.0版本的C++样例。使用dvpp对视频帧进行预处理，对预处理后的视频帧进行口罩识别，最后进行相应后处理。 
- [for_atlas200dk_1.3x.0.0_python](https://gitee.com/ascend/samples/tree/master/mark_detection_video/for_atlas200dk_1.3x.0.0_python)：该分支是运行在200DK上基于1.3x.0.0版本的python样例。使用opencv对视频帧进行预处理，对预处理后的视频帧进行口罩识别，最后进行相应后处理。
</blockquote></details>

<details open><summary>Atlas200DK-UART-CAR：基于caffe-ssd的手势识别应用，输入为树莓派摄像头，输出为视频。</summary><blockquote>

- [for_atlas200dk_1.3x.0.0_c++](https://gitee.com/ascend/samples/tree/master/Atlas200DK-UART-CAR/for_atlas200dk_1.3x.0.0_c++)：该分支是运行在200DK上基于1.3x.0.0版本的C++样例。使用dvpp对视频帧进行预处理，对预处理后的视频帧进行手势识别，宠星星:
使用dvpp对画面帧进行预处理，对预处理的画面帧进行手势识别，最后将识别的结果经过蓝牙传递给小车。
</blockquote></details>
