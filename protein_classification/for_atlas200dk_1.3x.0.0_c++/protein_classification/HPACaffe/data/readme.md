### 这个文件夹下面存放的是训练和测试所用到的部分数据集

#### train文件夹下面是部分训练数据集，test文件夹下面是部分测试数据集。

#### 其中 train.txt 和 test.txt 是相应图片的label标签文件。

#### 全部的数据集请前往 http://www.proteinatlas.org/ 自行下载。